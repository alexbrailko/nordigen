const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const cors = require("cors");
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(bodyParser.json());

let rawData = fs.readFileSync("test_data.json");
let bankData = JSON.parse(rawData);

const port = process.env.PORT || 3002;

app.get("/categories", (req, res) => {
  // setTimeout to emulate server response delay
  setTimeout(() => {
    res.status(200).send(bankData.categories);
  }, 500);
});

app.get("/accounts", (req, res) => {
  setTimeout(() => {
    res.status(200).send(bankData.accounts);
  }, 500);
});

app.post("/transactions", (req, res) => {
  const { accountId, category, date } = req.body;

  if (!accountId) return res.status(400).send("Account not found");

  const transactions = bankData.transactions
    .filter((acc) => {
      if (category) {
        return acc.accountId === accountId && acc.category === category;
      } else {
        return acc.accountId === accountId;
      }
    })
    .sort((dateA, dateB) => {
      if (date === "desc") {
        return new Date(dateB.date) - new Date(dateA.date);
      } else {
        return new Date(dateA.date) - new Date(dateB.date);
      }
    });

  setTimeout(() => {
    res.status(200).send(transactions);
  }, 500);
});

app.listen(port, () => {
  console.log("server is running");
});

module.exports = {
  app,
  bankData,
};
