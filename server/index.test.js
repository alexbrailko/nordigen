import { expect, test, describe } from "vitest";
const request = require("supertest");
const app = require("./index").app;
const bankData = require("./index").bankData;

describe("/categories", () => {
  test("to return categories list", async () => {
    await request(app)
      .get("/categories")
      .expect(200)
      .expect((res) => {
        expect(res.body.length).toBeGreaterThan(0);
      });
  });
});

describe("/accounts", () => {
  test("to return accounts list", async () => {
    await request(app)
      .get("/accounts")
      .expect(200)
      .expect((res) => {
        expect(res.body.length).toBeGreaterThan(0);
        expect(res.body[0].id).toBeDefined();
        expect(res.body[0].iban).toBeDefined();
        expect(res.body[0].owner).toBeDefined();
      });
  });
});

describe("/transactions", () => {
  test("to return transactions list", async () => {
    const data = {
      accountId: bankData.accounts[0].id,
      category: bankData.categories[0],
    };
    await request(app)
      .post("/transactions")
      .send(data)
      .expect(200)
      .expect((res) => {
        expect(res.body.length).toBeGreaterThan(0);
        expect(res.body[0].id).toBeDefined();
        expect(res.body[0].accountId).toBeDefined();
        expect(res.body[0].category).toBeDefined();
        expect(res.body[0].amount).toBeDefined();
        expect(res.body[0].date).toBeDefined();
        expect(res.body[0].currency).toBeDefined();
      });
  });

  test("to throw 400 error if account id is not passed", async () => {
    await request(app).post("/transactions").send({}).expect(400);
  });

  test("to check transactions being sorted in reverse date order", async () => {
    const data = {
      accountId: bankData.accounts[0].id,
      category: bankData.categories[0],
      date: "desc",
    };
    await request(app)
      .post("/transactions")
      .send(data)
      .expect(200)
      .expect((res) => {
        expect(res.body.length).toBeGreaterThan(1);
        expect(new Date(res.body[0].date).getTime()).toBeGreaterThan(
          new Date(res.body[1].date).getTime()
        );
      });
  });
});
