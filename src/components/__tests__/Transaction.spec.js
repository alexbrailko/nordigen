import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import Transaction from "@/components/Transaction.vue";

describe("Transaction", () => {
  const props = {
    amount: "100",
    date: "2022-08-17T23:43:18.753Z",
    currency: "USD",
    category: "Other",
  };
  const wrapper = mount(Transaction, {
    props,
  });

  it("renders properly", () => {
    expect(wrapper.find("div.amount").text()).toBe("100 USD");
    expect(wrapper.find("div.amount").classes()).toContain("balancePositive");
    expect(wrapper.find("div.category").text()).toBe("Other");
    wrapper.vm.formatDate(props.date);
    expect(wrapper.find("div.date").text()).toBe("18/08/2022");
  });
});
