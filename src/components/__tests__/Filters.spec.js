import { describe, it, expect, afterAll, beforeEach } from "vitest";
import { axiosInstance } from "@/main";
import { mount } from "@vue/test-utils";
import Filters from "@/components/Filters.vue";

const MockAdapter = require("axios-mock-adapter");
const mock = new MockAdapter(axiosInstance);

describe("Filters", () => {
  const categories = [
    {
      name: "Transfer",
      value: "Transfer",
    },
    {
      name: "Shopping",
      value: "Shopping",
    },
  ];

  afterAll(() => {
    mock.restore();
  });
  beforeEach(() => {
    mock.reset();
  });

  mock.onGet("/categories").reply(200, categories);

  const wrapper = mount(Filters, {
    props: { selectedAccount: "123" },
  });

  it("saves categories to state", () => {
    expect(wrapper.vm.$data.categories.length).toBe(2);
  });

  it("changes categoryKey and dateKey values on prop change", async () => {
    await wrapper.setProps({ selectedAccount: "1234" });
    expect(wrapper.vm.categoryKey).toBe(1);
    expect(wrapper.vm.dateKey).toBe(1);
  });
});
