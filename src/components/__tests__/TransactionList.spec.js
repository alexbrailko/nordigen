import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import TransactionList from "@/components/TransactionList.vue";

describe("TransactionList", () => {
  const transactions = [
    {
      id: "26d62c3c-e817-4642-b99b-3d08626d51b9",
      accountId: "0e734291-2a88-4dc7-97ea-b2a49678b826",
      category: "Utilities",
      amount: "100",
      date: "2022-08-17T23:43:18.753Z",
      currency: "EUR",
    },
    {
      id: "3e8c76c4-9434-4cfb-a76f-61e323432810",
      accountId: "0e734291-2a88-4dc7-97ea-b2a49678b826",
      category: "Shopping",
      amount: "200",
      date: "2022-04-27T04:06:21.906Z",
      currency: "EUR",
    },
  ];
  const props = {
    loading: false,
    transactions,
  };
  const wrapper = mount(TransactionList, {
    props,
  });

  it("shows loading state", () => {
    const wrapper = mount(TransactionList, {
      props: {
        loading: true,
        transactions,
      },
    });
    expect(wrapper.find("div.overlay").attributes("style")).not.toBe(
      "display: none;"
    );
  });

  it("changes total amount value", async () => {
    await wrapper.vm.getTotalAmount(transactions);
    expect(wrapper.vm.$data.totalAmount).toBe(300);
    expect(wrapper.find(".total").text()).toBe("Total: 300 EUR");
  });
});
