import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import Select from "@/components/Select.vue";

describe("Select", () => {
  const options = [
    {
      name: "Test",
      value: 1,
    },
    {
      name: "Test2",
      value: 2,
    },
  ];
  const wrapper = mount(Select, {
    props: { options, showDefaultName: false },
  });
  const select = wrapper.find("select");

  it("renders properly", () => {
    expect(select.exists()).toBeTruthy();
    expect(select.findAll("option").at(0).attributes("value")).toBe("1");
    expect(select.findAll("option").at(0).text()).toBe("Test");
  });

  it("emits change event", () => {
    wrapper.findAll("option").at(1).trigger("change");
    expect(wrapper.emitted("selectedValue")).toHaveLength(1);
  });
});
