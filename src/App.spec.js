import { describe, it, expect, afterAll, beforeEach } from "vitest";
import { axiosInstance } from "@/main";
import { shallowMount } from "@vue/test-utils";
import App from "@/App.vue";

const MockAdapter = require("axios-mock-adapter");
const mock = new MockAdapter(axiosInstance);

describe("Filters", () => {
  const accounts = [
    {
      name: "John - 123",
      value: "12345",
    },
    {
      name: "Nancy - 345",
      value: "897",
    },
  ];

  const transactions = [
    {
      id: "26d62c3c-e817-4642-b99b-3d08626d51b9",
      accountId: "0e734291-2a88-4dc7-97ea-b2a49678b826",
      category: "Utilities",
      amount: "100",
      date: "2022-08-17T23:43:18.753Z",
      currency: "EUR",
    },
    {
      id: "3e8c76c4-9434-4cfb-a76f-61e323432810",
      accountId: "0e734291-2a88-4dc7-97ea-b2a49678b826",
      category: "Shopping",
      amount: "200",
      date: "2022-04-27T04:06:21.906Z",
      currency: "EUR",
    },
  ];

  afterAll(() => {
    mock.restore();
  });
  beforeEach(() => {
    mock.reset();
  });

  mock.onGet("/accounts").reply(200, accounts);

  const wrapper = shallowMount(App);

  it("saves accounts data to state", () => {
    expect(wrapper.vm.$data.accounts.length).toBe(2);
  });

  it("saves transactions data to state", async () => {
    mock.onPost("/transactions").reply(200, transactions);
    await wrapper.vm.getTransactions("123", "Other", "asc");
    expect(wrapper.vm.$data.transactions.length).toBe(2);
  });
});
