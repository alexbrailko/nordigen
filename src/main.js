import { createApp } from "vue";
import axios from "axios";
import App from "./App.vue";

import "./assets/scss/styles.scss";

export const axiosInstance = axios.create({
  baseURL: "http://localhost:3002",
});

const app = createApp(App);

app.mount("#app");
