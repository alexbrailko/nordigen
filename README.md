## Install dependencies in root and server folder

```sh
npm install
cd server
npm install
cd ..
```

## Run project in dev mode. Will start client and server simultaneously

```sh
npm run dev
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```
